from enum import Enum

from twisted.internet import reactor, task
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Factory

import sys
import signal
import queue
import logging as log
import time
import pdb

EVENT_ARGUMENTS = ['sequence', 'action', 'src_user', 'dest_user']
# Minimum number of events in the queue before we dispatch
EVENT_QUEUE_FIRE_THRESHOLD = 1000

class ProtocolType(Enum):
    EVENT_SRC = 0
    CLIENT    = 1

class ClientConnection(LineReceiver):
    """
    Registers usernames and maintains open connections with clients.
    """

    def __init__(self, users):
        self.delimiter = b'\n'
        self.users     = users
        self.userId    = None

    def connectionMade(self):
        log.info("New client connection.")

    def connectionLost(self, reason):
        log.info('User ' + str(self.userId) + ' disconnected.')
        #self.users[self.userId] = None
        del self.users[self.userId]

    def lineReceived(self, line):
        username = line.decode('utf-8').rstrip()
        log.info('Registering user ' + username)
        self.userId = username
        self.users[username] = self

    def sendLine(self, line):
        log.debug("Sending line to user " + str(self.userId) + ": " + str(line.decode("utf-8")))
        line += self.delimiter
        self.transport.write(line)

class EventSrcListener(LineReceiver):
    """
    Listen for and parse events, taking care to store them in order by sequence.
    Then, every so often, send batches of in-order events to the event dispatcher.
    """

    def __init__(self, eventDispatcher):
        self.delimiter = b'\n'
        self.eventDispatcher = eventDispatcher

        # A priority queue used to untangle the order of incoming events.
        self.eventQueue = queue.PriorityQueue(maxsize=EVENT_QUEUE_FIRE_THRESHOLD*10)
        # The last time that we sent events to our clients
        self.lastDispatchTime = None

        queueMonitor = task.LoopingCall(self.dispatchEventsIfStale)
        queueDeferred = queueMonitor.start(3)
        queueDeferred.addErrback(self.staleDispatchError)

    def connectionMade(self):
        log.info('Connected to event source. Receiving events...')

    def connectionLost(self, reason):
        log.info('Event source connection closed. Reason: ')
        log.info(reason)

    def lineReceived(self, line):
        """
        Receive, decode, and parse events. Put them inside a priority queue for
        sorting (see fireQueue())
        """
        raw = line.decode('utf-8').rstrip()
        log.debug('Received event: ' + raw)
        event = parseEvent(raw)

        self.eventQueue.put((event['sequence'], event))

        if self.eventQueue.qsize() >= EVENT_QUEUE_FIRE_THRESHOLD:
            self.fireQueue()

    def fireQueue(self):
        """
        Accumulate events in a priority queue until we reach some arbitary
        threshold. Then, empty out the queue until we find a "gap" (two sequence
        numbers with a difference > 1). Events after the gap stay inside of the
        queue -- we need to wait for the missing events to arrive.

        Example:

        queue front --                gap
                      \                  \
                      [382, 383, 384, 385, 453, 454]

        The queue fires 382 through 385, leaving 453 and 454.
                      [453, 454]

        More events arrive later, filling the gap.

                      [386, 387, 388, ... , 452, 453, 454]

        The queue will then fire all of the events.

        This mechanism guarantees that all events arrive in order.
        """
        foundGap  = False
        prevSeq   = None
        prevEvent = None
        toDispatch = []
        
        while not self.eventQueue.empty() and not foundGap:
            if not prevSeq:
                prevSeq, prevEvent = self.eventQueue.get()
            currSeq, currEvent = self.eventQueue.get()
            if currSeq - prevSeq == 1:
                toDispatch.append(prevEvent)
                prevSeq   = currSeq
                prevEvent = currEvent
                
                # if there is no "next" entry, we have to dispatch the current
                # event
                if self.eventQueue.empty():
                    toDispatch.append(currEvent)
            else:
                foundGap = True
                log.debug("Found gap: prev curr " + str(prevSeq) + " " + str(currSeq))
                toDispatch.append(prevEvent)
                self.eventQueue.put((currSeq, currEvent))

        self.eventDispatcher.dispatch(toDispatch)
        self.lastDispatchTime = time.time()       

    def dispatchEventsIfStale(self):
        """
        After events stop arriving (either because the event source has stopped
        publishing events, or because of a long period of latency), there may be
        some residual events left in the event queue. To solve this problem, we
        check the last time that events were dispatched to clients. If too much
        time has passed, send residual events to the dispatcher.
        """
        log.info("Monitoring event queue...")
        if self.lastDispatchTime and self.eventQueue.qsize() > 0:
            if time.time() - self.lastDispatchTime > 5:
                log.info('Last dispatch was over 5 seconds ago. Emptying event queue.')
                self.fireQueue()

    def staleDispatchError(self, failure):
        log.error('Stale dispatch timer error: ')
        log.error(str(failure))

def parseEvent(event):
    args = event.split('|')
    parsedEvent = {x : None for x in EVENT_ARGUMENTS}
    parsedEvent['sequence'] = int(args[0])
    parsedEvent['action']   = args[1]
    if len(args) > 2:
        parsedEvent['src_user'] = args[2]
    if len(args) > 3:
        parsedEvent['dest_user'] = args[3]

    return parsedEvent

def encodeEvent(parsedEvent):
    encodedEvent = ''
    encodedEvent += str(parsedEvent['sequence']) + '|'
    encodedEvent += parsedEvent['action']
    if parsedEvent['src_user'] is not None:
        encodedEvent += '|' + parsedEvent['src_user']
    if parsedEvent['dest_user'] is not None:
        encodedEvent += '|' + parsedEvent['dest_user']
    encodedEvent = encodedEvent.encode("utf-8")
    
    return encodedEvent

class EventDispatcher(object):
    """
    Forward events to their destinations.
    """

    def __init__(self, users):

        # Map users to set of followers
        self.followers = {}
        self.users = users
        self.totalDispatched = 0

        self.actionTable = {
            'F' : self.follow,
            'U' : self.unfollow,
            'B' : self.broadcast,
            'P' : self.privateMsg,
            'S' : self.statusUpdate
        }

    def dispatch(self, eventList):
        for event in eventList:
            action = event['action']
            # Handle the event
            self.actionTable[action](event)
        self.totalDispatched += len(eventList)

        if log.getLogger().getEffectiveLevel() == log.DEBUG:
            log.debug('Dispatching ' + str(len(eventList)) + ' events')
            for idx, x in enumerate(eventList):
                seq = x['sequence']
                log.debug(str(idx) + ': dispatching seq ' + str(seq))

    def follow(self, event):
        encoded = encodeEvent(event)
        src_user  = event['src_user']
        dest_user = event['dest_user']
        if dest_user in self.followers:
            self.followers[dest_user].add(src_user)
        else:
            followerSet = set()
            followerSet.add(src_user)
            self.followers[dest_user] = followerSet

        if dest_user in self.users:
            clientConn = self.users[dest_user]
            clientConn.sendLine(encoded)

    def unfollow(self, event):
        src_user = event['src_user']
        dest_user = event['dest_user']
        if dest_user in self.followers:
            if src_user in self.followers[dest_user]:
                self.followers[dest_user].remove(src_user)

    def broadcast(self, event):
        encoded = encodeEvent(event)
        for clientConn in self.users.values():
            clientConn.sendLine(encoded)

    def privateMsg(self, event):
        encoded = encodeEvent(event)
        dest_user = event['dest_user']
        if dest_user in self.users:
            clientConn = self.users[dest_user]
            clientConn.sendLine(encoded)

    def statusUpdate(self, event):
        encoded = encodeEvent(event)
        src_user = event['src_user']
       
        if src_user in self.followers: 
            for follower in self.followers[src_user]:
                if follower in self.users:
                    clientConn = self.users[follower]
                    clientConn.sendLine(encoded)

class FwdServerFactory(Factory):
    """
    Builds protocols for communication between the single event source and many
    client connections.
    """
    # Map usernames to open connections
    users = {}

    def __init__(self, protocolType):
        self.protocolType = protocolType
        self.eventDispatcher = EventDispatcher(self.users)

    def buildProtocol(self, addr):
        if self.protocolType == ProtocolType.EVENT_SRC:
            return EventSrcListener(self.eventDispatcher)
        elif self.protocolType == ProtocolType.CLIENT:
            return ClientConnection(self.users)


def exit_gracefully(signal, frame):
    log.info('SIGINT received, shutting down.')
    reactor.stop()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, exit_gracefully)
    log.basicConfig(
                    filename='events.log',
                    format='%(asctime)s %(levelname)s: %(message)s',
                    level=log.INFO
                   )

    reactor.listenTCP(9090, FwdServerFactory(ProtocolType.EVENT_SRC))
    reactor.listenTCP(9099, FwdServerFactory(ProtocolType.CLIENT))

    log.info('=' * 80)
    log.info('Listening for event/client connections...')
    log.info('=' * 80)
    reactor.run()
