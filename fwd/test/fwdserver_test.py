from fwd.fwdserver import parseEvent, encodeEvent
from fwd.fwdserver import EventDispatcher

from twisted.trial import unittest
from twisted.test import proto_helpers

import os
import sys
import pdb

class EventParsingTest(unittest.TestCase):
    
    def test_parse_broadcast(self):
        rawEvent = '29382|B'
        expected = {
                'sequence'  : 29382,
                'action'    : 'B',
                'src_user'  : None,
                'dest_user' : None
            }

        actual = parseEvent(rawEvent)
        self.assertEqual(expected, actual)

    def test_parse_follow(self):
        rawEvent = '28759|F|29|98'
        expected = {
                'sequence'  : 28759,
                'action'    : 'F',
                'src_user'  : '29',
                'dest_user' : '98'
            }

        actual = parseEvent(rawEvent)
        self.assertEqual(expected, actual)

    def test_parse_unfollow(self):
        rawEvent = '28759|U|29|98'
        expected = {
                'sequence'  : 28759,
                'action'    : 'U',
                'src_user'  : '29',
                'dest_user' : '98'
            }

        actual = parseEvent(rawEvent)
        self.assertEqual(expected, actual)

    def test_parse_private_msg(self):
        rawEvent = '28759|P|29|98'
        expected = {
                'sequence'  : 28759,
                'action'    : 'P',
                'src_user'  : '29',
                'dest_user' : '98'
            }

        actual = parseEvent(rawEvent)
        self.assertEqual(expected, actual)

    def test_parse_status_update(self):
        rawEvent = '28759|S|29'
        expected = {
                'sequence'  : 28759,
                'action'    : 'S',
                'src_user'  : '29',
                'dest_user' : None
            }

        actual = parseEvent(rawEvent)
        self.assertEqual(expected, actual)

class EventDispatcherTest(unittest.TestCase):
   
    def setUp(self):
        self.user1 = FakeConnection('98')
        self.user2 = FakeConnection('22')
        self.user3 = FakeConnection('10')
        
        self.users = {}
        self.users[self.user1.userId] = self.user1
        self.users[self.user2.userId] = self.user2
        self.users[self.user3.userId] = self.user3

        self.eventDispatcher = EventDispatcher(self.users) 

    def test_dispatch_follow(self):
        follow = {
                'sequence'  : 28759,
                'action'    : 'F',
                'src_user'  : '22',
                'dest_user' : '98'
            }
        eventList = [follow]
        self.eventDispatcher.dispatch(eventList)
        self.assertEqual(b"28759|F|22|98", self.users['98'].received[0])
        self.assertEqual(self.users['10'].received, [])

    def test_dispatch_unfollow(self):
        unfollow = {
                'sequence'  : 28759,
                'action'    : 'U',
                'src_user'  : '22',
                'dest_user' : '98'
            }
        eventList = [unfollow]
        self.eventDispatcher.dispatch(eventList)
        self.assertEqual(self.users['98'].received, [])
        self.assertEqual(self.users['22'].received, [])

    def test_dispatch_broadcast(self):
        broadcast = {
                'sequence'  : 28759,
                'action'    : 'B',
                'src_user'  : None,
                'dest_user' : None
            }
        eventList = [broadcast]
        self.eventDispatcher.dispatch(eventList)
        self.assertEqual(b"28759|B", self.users['98'].received[0])
        self.assertEqual(b"28759|B", self.users['22'].received[0])
        self.assertEqual(b"28759|B", self.users['10'].received[0])

    def test_dispatch_private_msg(self):
        private = {
                'sequence'  : 28759,
                'action'    : 'P',
                'src_user'  : '22',
                'dest_user' : '98'
            }
        eventList = [private]
        self.eventDispatcher.dispatch(eventList)
        self.assertEqual(b"28759|P|22|98", self.users['98'].received[0])
        self.assertEqual(self.users['10'].received, [])
        self.assertEqual(self.users['22'].received, [])

    def test_dispatch_status_update(self):
        follow = {
                'sequence'  : 28759,
                'action'    : 'F',
                'src_user'  : '22',
                'dest_user' : '98'
            }
        status = {
                'sequence'  : 28760,
                'action'    : 'S',
                'src_user'  : '98',
                'dest_user' : None
            }
        
        eventList = [follow, status]
        self.eventDispatcher.dispatch(eventList)
        self.assertEqual(b"28760|S|98", self.users['22'].received[0])
        self.assertEqual(self.users['10'].received, [])

# XXX: This should be mocked from ClientConnecion
class FakeConnection(object):
    def __init__(self, userId):
        self.userId   = userId
        self.received = []

    def sendLine(self, line):
        self.received.append(line)
