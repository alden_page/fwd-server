# Longer-running integration test. Tests 1,000,000 events.
python3 ../fwdserver.py &
serverPid=$!

totalEvents=1000000
./followermaze.sh

kill -9 $serverPid
