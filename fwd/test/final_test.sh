# Longer-running integration test. Tests 1,000,000 events.
python3 ../fwdserver.py &
serverPid=$!

totalEvents=10000000
./followermaze.sh

kill -9 $serverPid
